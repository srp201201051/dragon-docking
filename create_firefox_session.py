import json

from selenium import webdriver
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities
from selenium.webdriver.firefox.options import Options


def dump_session_data(driver):
    session_data = {}
    session_data['session_url'] = driver.command_executor._url
    session_data['session_id'] = driver.session_id
    json.dump(session_data, open("session_data.json", 'w'))


def open_firefox_session():
    firefox_capabilities = DesiredCapabilities.FIREFOX
    firefox_capabilities['marionette'] = True
    options = Options()
    options.add_argument("disable-infobars")
    # options.headless = True
    driver = webdriver.Firefox(
        capabilities=firefox_capabilities,
        options=options
    )
    return driver


if __name__ == "__main__":
    driver = open_firefox_session()
    driver.set_page_load_timeout(600)
    driver.get("https://iss-sim.spacex.com")
    dump_session_data(driver)
    print("Session data dumped.")
    print("")
    print("-- Type EXIT and press enter to exit gracefully --")
    while input("> ") != "EXIT":
        continue
    driver.quit()
